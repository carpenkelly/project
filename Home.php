<!DOCTYPE html>
<html lang="en">

<title>Project Kelly Carpenter</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel ="stylesheet" href="css/Styles.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}
</style>
<body>
<!-- Should have named page1 home -->
<<!-- Navbar -->
<div class="w3-top">
<ul class="w3-navbar w3-black w3-card-2 w3-left-align">
  <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
    <a class="w3-padding-large" href="javascript:void(0)" onclick="myFunction()" title="Toggle Navigation Menu"><em class="fa fa-bars"></em></a>
  </li>
  <li><a href="Home.php" class="w3-hover-none w3-hover-text-grey w3-padding-large">Insert</a></li>
  <li class="w3-hide-small"><a href="Select.php" class="w3-padding-large">Select</a></li>
  <li class="w3-hide-small"><a href="Update.php" class="w3-padding-large">Update</a></li>
  
  <li class="w3-hide-small w3-dropdown-hover">
    <a href="javascript:void(0)" class="w3-hover-none w3-padding-large" title="More">Queries<em class="fa fa-caret-down"></em></a>     
    <div class="w3-dropdown-content w3-white w3-card-4">
      <a href="Query1.php">Query 1</a>
      <a href="Query2.php">Query 2</a>
      <a href="Query3.php">Query 3</a>
    </div>
  </li>
  <li class="w3-hide-small"><a href="References.php" class="w3-padding-large">References</a></li>
  <li class="w3-hide-small w3-right"><a href="javascript:void(0)" class="w3-padding-large w3-hover-red"><em class="fa fa-search"></em></a></li>
</ul>
</div>

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-hide w3-hide-large w3-hide-medium w3-top" style="margin-top:46px">
  <ul class="w3-navbar w3-left-align w3-black">
    <li><a class="w3-padding-large" href="Select.php">Select</a></li>
    <li><a class="w3-padding-large" href="Update.php">Update</a></li>
    <li><a class="w3-padding-large" href="Query1.php">Query 1</a></li>
    <li><a class="w3-padding-large" href="Query2.php">Query 2</a></li>
	<li><a class="w3-padding-large" href="Query3.php">Query 3</a></li>
    <li><a class="w3-padding-large" href="References.php">References</a></li>
  </ul>
</div>
<!-- Header -->
<header class="w3-container w3-center w3-padding-75">
<img src="img/logo.png" alt="logo">

  <h1 class="w3-margin w3">Kelly Carpenter - Project 1</h1>
  <p class="w3-xlarge"></p>
  
</header>

<!-- First Grid -->
<div class="w3-row-padding w3-padding-64 w3-container">
  <div class="w3-content">
    <div class="w3-twothird">
       <h2> Insert Data for tblUsers</h2>
    <h3 class="w3-padding-5">This page will insert users into tblUsers.</h3>
<?php

// define variables and set to empty values

$nameErr = $addressErr  = $emailErr = $phoneErr= "";
$nameStar = $addressStar = $emailStar = $phoneStar = "noerror";
$name = $phone = $address = $email = "";


$errflag = false; // If there is any error, this is set to true

// =============================== Validation Block ==================================
// validate user's input stored in postback

if ($_SERVER["REQUEST_METHOD"] == "POST")
	{

	// validate name field. It is required. Notice if it passes the required
	// test, we then check it for format. Set the errflag is an error is detected.

	if (empty($_POST["name"]))
		{
		$nameErr = "Name is required";
		$errflag = true; // setting errflag to true for any error
		$nameStar = "error";
		}
	  else
		{
		$name = test_input($_POST["name"]); // store postback entry in name variable
		$nameStar = "noerror";

		// check if name only contains letters and whitespace

		if (!preg_match("/^[a-zA-Z ]*$/", $name))
			{
			$nameErr = "Only letters and white space allowed";
			$errflag = true;
			$nameStar = "error";
			}
		}

	// -----------------------------------------------------------------------------
	// validate phone field. It is required. If it is present, we then check for the
	// proper format. We used a pattern match to check the phone format. Make sure to
	// tell the user what the proper format is in the form.
	if (empty ($_POST["address"]))
	{
		$addressErr = "Address is required";
		$errflag = true;
		$addressStar = "error";
	}
		else 
		{
			$address = test_input($_POST["address"]);
			$addressStar = "noerror";
			$regex = '/[A-Za-z0-9\-\\,.]+/';
		if(preg_match($regex, $address)) {
			$address = $address;
			$addressStar = "noerror";
		} else {
			$addressErr = 'Street address must be valid';
			$errflag = true;
			$addressStar = "error";
		}  
	}
		if (empty ($_POST["email"]))
	{
		$emailErr = "Email is required";
		$errflag = true;
		$emailStar = "error";
	}
		else 
		{
			$email = test_input($_POST["email"]);
			$emailStar = "noerror";
			
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
		{
			$emailErr = "Invalid email format"; 
			$errflag = true;
			$emailStar = "error";
			
			} 
		}
		
		
	if (empty($_POST["phone"]))
		{
		$phoneErr = "Phone number is required";
		$errflag = true;
		$phoneStar = "error";
			}
	  else
		{
		$phone = test_input($_POST["phone"]);
		$phoneStar = "noerror";

		// The following regular expression is rather restrictive. It will only allow two phone formats
		// (555)555-5555 | (555) 555-5555

		if (!preg_match("/[\(]\d{3}[\)][\s]?\d{3}[\-]\d{4}/", $phone))
			{
			$phoneErr = " Invalid Phone Format: (555)555-5555";
			$errflag = true;
		    $phoneStar = "error";
			echo "<br /> Phone number entered is $phone <br />";
			$ans = preg_match("/[\(]\d{3}[\)][\s]?\d{3}[\-]\d{4}/", $phone);
			// echo "Preg match = $ans";     // this was diagnostic output. I used it to track what was going on to debug the code. At the end I removed it. 
			}


		if (!preg_match("/^(?:1(?:[. -])?)?(?:\((?=\d{3}\)))?([2-9]\d{2})(?:(?<=\(\d{3})\))? ?(?:(?<=\d{3})[.-])?([2-9]\d{2})[. -]?(\d{4})(?: (?i:ext)\.? ?(\d{1,5}))?$/", $phone))
			{
			$phoneErr = "Invalid Phone Format test 2: (555)555-5555";
			$errflag = true;
		    $phoneStar = "error";
			}
	}
	}
// -----------------------------------------------------------------------------

 
 // this is the end of the validation block  - SERVER["REQUEST_METHOD"] == "POST")

// function used in cleaning data
function test_input($data)
	{
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
	}
	
// ===============================End of Validation Block ========================
?>
<!-- ============================Start of HTML Table Block ======================= -->
<p> <span class="error">* Required field </span> </p>

<!-- post back to the php file -->
<form method="post" action="<?php
echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">

<table>
<tr>
<th colspan="2">
User Information
</th>
</tr>
<tr>
<td>
<!-- Need to include the value attribute, so it will reload the user input on postback 

Note, I modified this so the error message is not included in the table itself so that the layout of the table does not change. I did change the color of the astrick to red if there was an error, and add the error messages to below the table. -->


Name: <span class="<?php echo $nameStar; ?>">* </span></td>
<td> <input type = "text" name="name" value ="<?php
echo $name; ?>" placeholder = "Name Required">
</td>
</tr>
<tr>
<td>
Address: <span class="<?php echo $addressStar; ?>">* </span></td>
<td> <input type = "text" name="address" placeholder = "Address Required" value ="<?php
echo $address; ?>" >
</td>
</tr>
<tr>
<td>
Email: <span class="<?php echo $emailStar; ?>">* </span></td>
<td> <input type = "text" name="email" placeholder = "Email required" value ="<?php 
echo $email; ?>" >
</td>
</tr>
<tr>
<td>
Phone: <span class="<?php
echo $phoneStar; ?>">* </span></td><td>
<!-- Need to include the value attribute, so it will reload the user input on postback. 
     Placeholder is useful to give the user a hint as to what format to enter. This is
	 an HTML feature, not PHP.-->
<input type="text" name="phone" placeholder="(555)555-1234" value="<?php
echo $phone; ?>">
</td>
</tr>
<tr>
<td colspan="2">
<input type="submit" name="submit" value="Submit"> 
</td>
</tr>
</table>
</form>



</div>
</body>
</html>
    <div class="w3-third w3-center">
      
    </div>
  </div>
</div>

<!-- Second Grid -->


<div class="w3-container w3-black w3-center w3-opacity w3-padding-64">
    <h1 class="w3-margin w3-xlarge"></h1>
	<?php
// ======================== output ==========================
$spanerr = "<span class='error'>";   // set the color red
$spanend = "</span>";

if ($_SERVER["REQUEST_METHOD"] == "POST")
	{
		// If there is an error (errflag = true), show the error text
	if ($errflag)
		{

		echo "<h2>". $spanerr."Please correct errors".$spanend."</h2>";
		echo "<ul>";
		if (strlen($nameErr) > 0)
			{
			echo "<li> $spanerr.$nameErr.$spanend </li>";
			}
		if (strlen($phoneErr) > 0)
			{
			echo "<li> $spanerr.$phoneErr.$spanend </li>";
			}
		if (strlen($addressErr) > 0)
			{
			echo "<li> $spanerr.$addressErr.$spanend </li>";
			}
		if (strlen($emailErr) > 0)
			{
			echo "<li> $spanerr.$emailErr.$spanend </li>";
			}
		}
	// No error, so show the user input.	
	  else
		{	   
	// Output the user Data
		echo "<h2>Valid Input:</h2>";
		echo "Name: " . $name;
		echo "<br />";
		echo "Address: " . $address;
		echo "<br />";
		echo "Email: " . $email;
		echo "<br />";
		echo "Phone: " . $phone;
		echo "<br />";
		
	
$servername = "localhost";
$username = "username";
$password = "password";

try {
    $conn = new PDO("mysql:host=$servername;dbname=sakila", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 
	$sql = "INSERT INTO tblUsers (name, address, email, phone)
    VALUES ('$name', '$address', '$email', '$phone')";
    // use exec() because no results are returned
    $conn->exec($sql);
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }
	}
	}
?>
</div>

<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-opacity">

 <p></a></p>
</footer>

<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>

</body>
</html>


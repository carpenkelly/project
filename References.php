<!DOCTYPE html>
<html lang="en">
<title>Project Kelly Carpenter</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel ="stylesheet" href="css/Styles.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}
</style>
<body>
<!-- Navbar -->
<div class="w3-top">
<ul class="w3-navbar w3-black w3-card-2 w3-left-align">
  <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
    <a class="w3-padding-large" href="javascript:void(0)" onclick="myFunction()" title="Toggle Navigation Menu"><em class="fa fa-bars"></em></a>
  </li>
  <li><a href="Home.php" class="w3-hover-none w3-hover-text-grey w3-padding-large">Insert</a></li>
  <li class="w3-hide-small"><a href="Select.php" class="w3-padding-large">Select</a></li>
  <li class="w3-hide-small"><a href="Update.php" class="w3-padding-large">Update</a></li>
  
  <li class="w3-hide-small w3-dropdown-hover">
    <a href="javascript:void(0)" class="w3-hover-none w3-padding-large" title="More">Queries<em class="fa fa-caret-down"></em></a>     
    <div class="w3-dropdown-content w3-white w3-card-4">
      <a href="Query1.php">Query 1</a>
      <a href="Query2.php">Query 2</a>
      <a href="Query3.php">Query 3</a>
    </div>
  </li>
  <li class="w3-hide-small"><a href="References.php" class="w3-padding-large">References</a></li>
  <li class="w3-hide-small w3-right"><a href="javascript:void(0)" class="w3-padding-large w3-hover-red"><em class="fa fa-search"></em></a></li>
</ul>
</div>

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-hide w3-hide-large w3-hide-medium w3-top" style="margin-top:46px">
  <ul class="w3-navbar w3-left-align w3-black">
    <li><a class="w3-padding-large" href="Select.php">Select</a></li>
    <li><a class="w3-padding-large" href="Update.php">Update</a></li>
    <li><a class="w3-padding-large" href="Query1.php">Query 1</a></li>
    <li><a class="w3-padding-large" href="Query2.php">Query 2</a></li>
	<li><a class="w3-padding-large" href="Query3.php">Query 3</a></li>
    <li><a class="w3-padding-large" href="References.php">References</a></li>
  </ul>
</div>

<!-- Header -->
<header class="w3-container w3-center w3-padding-75">
<img src="img/logo.png" alt="logo">

  <h1 class="w3-margin w3">Kelly Carpenter - Project 1</h1>
  <p class="w3-xlarge"></p>
  
</header>

<!-- First Grid -->
<div class="w3-row-padding w3-padding-64 w3-container">
  <div class="w3-content">
    <div class="w3-twothird">
           <h2> References</h2>
    <h3 class="w3-padding-5">Various references used throughout the project.</h3>
<p><a href="http://www.w3schools.com/w3css/tryw3css_templates_start_page.htm">Website Template</a></p>
<p><a href="http://www.w3schools.com/w3css/tryw3css_templates_band.htm">Navbar Template</a></p>
<p><a href="http://achecker.ca/checker/index.php">Accessibility Checker</a></p>

<p><a href="http://phpcodechecker.com/">PHP Checker</a></p>
<p><a href="https://coolors.co/">Color Scheme</a></p>
<p><a href="https://validator.w3.org/">HTML Checker</a></p>
<p><a href="https://jigsaw.w3.org/css-validator/">CSS Checker</a></p>

</div>
</div>
</div>


<!-- Second Grid -->

<!-- Footer -->


<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>

</body>
</html>
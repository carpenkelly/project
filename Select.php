<!DOCTYPE html>
<html lang="en">
<title>Project Kelly Carpenter</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel ="stylesheet" href="css/Styles.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}
</style>
<body>

<!-- Navbar -->
<div class="w3-top">
<ul class="w3-navbar w3-black w3-card-2 w3-left-align">
  <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
    <a class="w3-padding-large" href="javascript:void(0)" onclick="myFunction()" title="Toggle Navigation Menu"><em class="fa fa-bars"></em></a>
  </li>
  <li><a href="Home.php" class="w3-hover-none w3-hover-text-grey w3-padding-large">Insert</a></li>
  <li class="w3-hide-small"><a href="Select.php" class="w3-padding-large">Select</a></li>
  <li class="w3-hide-small"><a href="Update.php" class="w3-padding-large">Update</a></li>
  
  <li class="w3-hide-small w3-dropdown-hover">
    <a href="javascript:void(0)" class="w3-hover-none w3-padding-large" title="More">Queries<em class="fa fa-caret-down"></em></a>     
    <div class="w3-dropdown-content w3-white w3-card-4">
      <a href="Query1.php">Query 1</a>
      <a href="Query2.php">Query 2</a>
      <a href="Query3.php">Query 3</a>
    </div>
  </li>
  <li class="w3-hide-small"><a href="References.php" class="w3-padding-large">References</a></li>
  <li class="w3-hide-small w3-right"><a href="javascript:void(0)" class="w3-padding-large w3-hover-red"><em class="fa fa-search"></em></a></li>
</ul>
</div>

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-hide w3-hide-large w3-hide-medium w3-top" style="margin-top:46px">
  <ul class="w3-navbar w3-left-align w3-black">
    <li><a class="w3-padding-large" href="Select.php">Select</a></li>
    <li><a class="w3-padding-large" href="Update.php">Update</a></li>
    <li><a class="w3-padding-large" href="Query1.php">Query 1</a></li>
    <li><a class="w3-padding-large" href="Query2.php">Query 2</a></li>
	<li><a class="w3-padding-large" href="Query3.php">Query 3</a></li>
    <li><a class="w3-padding-large" href="References.php">References</a></li>
  </ul>
</div>

<!-- Header -->
<header class="w3-container w3-center w3-padding-75">
<img src="img/logo.png" alt="logo">

  <h1 class="w3-margin">Kelly Carpenter - Project 1</h1>
  <p class="w3-xlarge"></p>
  
</header>

<!-- First Grid -->
<div class="w3-row-padding w3-padding-64 w3-container">
  <div class="w3-content">
    <div class="w3-twothird">
     
    

       <h2>Search for customers by Store ID and Last Initial</h2>
     <h3 class="w3-padding-32">This page shows all of the customers with an inputed last initial by store id from the customer table.</h3>





<?php
$store_id = $last_initial = "";
$store_idErr = $last_initialErr = "";
$errFlag = false;

if ($_SERVER["REQUEST_METHOD"] == "POST")
	{
	if (empty($_POST["store_id"]))
		{
		$errFlag = true;
		$store_idErr = "Invalid store id";
		}
	  else
		{
		$store_id = test_input($_POST["store_id"]); 
		if (!filter_var($store_id, FILTER_VALIDATE_INT) === true) {
		$store_idErr = "Only Variables allowed";
		$errFlag = true;
		} 
		}

	if (empty($_POST["last_initial"]))
		{
		$last_initialErr = "Initial is required";
		$errFlag = true; // setting errflag to true for any error
		
		}
	  else
		{
		$last_initial = test_input($_POST["last_initial"]); // store postback entry in name variable
		

		// check if name only contains letters and whitespace

		if (!preg_match("/^[a-zA-Z ]*$/", $last_initial))
			{
			$last_initialErr = "Only letters and white space allowed";
			$errFlag = true;
		
			}
		}


	
	}
function test_input($data)
	{
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
	}
	
?>
<form method="post" action="<?php
echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">

<table>
<tr>
<th colspan="2">
User Information
</th>
</tr>
<tr>
<td>
<label for="store_id">Store ID:</label> <span class="<?php echo $store_id; ?>">* </span></td><td> <input type="text" name="store_id" id ="store_id"value="<?php
echo $store_id; ?>" placeholder = "">

</td>
</tr>
<tr>
<td>
<label for="last_initial">Last Initial:</label> <span class="<?php echo $last_initial; ?>">* </span></td>
<td> <input type = "text" name="last_initial" id ="last_initial" value =" <?php echo $last_initial; ?> "placeholder = ""></td>
</tr>
<tr>
<td colspan="2">
<input type="submit" name="submit" value="Submit"> 
</td>
</tr>
</table>
</form>


</div>
</body>
</html>
    <div class="w3-third w3-center">
      
    </div>
  </div>
</div>

<!-- Second Grid -->

<div class="w3-container w3-black w3-center w3-opacity w3-padding-64">
    <h1 class="w3-margin w3-xlarge"></h1>
	
<?php
// ======================== output ==========================
$spanerr = "<span class='error'>";   // set the color red
$spanend = "</span>";
if ($_SERVER["REQUEST_METHOD"] == "POST")
	{
		if ($errFlag){
			echo "<h2>". $spanerr."Please correct errors".$spanend."</h2>";
		echo "<ul>";
		if (strlen($store_idErr) > 0)
			{
			echo "<li> $spanerr.$store_idErr.$spanend </li>";
			}
		if (strlen($last_initialErr) > 0)
			{
			echo "<li> $spanerr.$last_initialErr.$spanend </li>";
			}
		}
		else{
		echo "<table> ";
		echo "<tr><th>Customer Id</th><th>Store ID</th><th><First Name</th><th>Last name</th><th>Email</th></tr>";
		
		
class TableRows extends RecursiveIteratorIterator { 
    function __construct($it) { 
        parent::__construct($it, self::LEAVES_ONLY); 
    }

    function current() {
        return "<td style='width:150px;border:1px solid black;'>" . parent::current(). "</td>";
    }

    function beginChildren() { 
        echo "<tr>"; 
    } 

    function endChildren() { 
        echo "</tr>" . "\n";
    }
}	


$servername = "localhost";
$username = "username";
$password = "password";
$dbname = "sakila";
$typedlastname = $last_initial . "%";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT customer_id, store_id, first_name, last_name, email FROM customer WHERE store_id like '$store_id' AND last_name like '$typedlastname'"); 
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC); 
    foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) { 
        echo $v;
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
	
$conn = null;
echo "</table>";
	}
	}
?>
</div>

<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-opacity">

 <p></a></p>
</footer>

<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>

</body>
</html>

